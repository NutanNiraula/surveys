//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport
import SurveysPlaygroundFramework
import Alamofire

//This playground is used for testing custom views and logics
//Always build SurveysPlaygroundFramework before testing

class MyViewController: UIViewController {
    
    func rotate(angle: CGFloat, view: UIView) {
        let radians = angle / 180.0 * CGFloat.pi
        let rotation = view.transform.rotated(by: radians)
        view.transform = rotation
    }
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let testView = TestView()
        print("sdasd \(UIScreen.main.bounds.height)")
        testView.frame = CGRect(x: 100, y: 300, width: 400, height: 50)
        rotate(angle: 90, view: testView)
        
        view.addSubview(testView)
    }
}

PlaygroundPage.current.liveView = MyViewController()
