//
//  JsonReader.swift
//  SurveysTests
//
//  Created by Nutan Niraula on 5/31/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

class JsonReader {
    
    static func createData(fromJSONFile fileName: String) -> Data {
        guard let path = TestBundle().returnPath(forResource: fileName, ofType: "json") else {
            fatalError("couldnt find path")
        }
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped) else {
            fatalError()
        }
        return data
    }
    
}
