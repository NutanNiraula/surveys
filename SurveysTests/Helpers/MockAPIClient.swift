//
//  MockAPIClient.swift
//  SurveysTests
//
//  Created by Nutan Niraula on 5/31/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation
@testable import Surveys

enum ApiResponse {
    case failure(jsonFileName: String)
    case success(jsonFileName: String)
}

class MockAPIClient: APIClient {
    
    var code: Int?
    
    var endPoint: EndPointProtocol

    var response: ApiResponse?
    
    private class FakeEndPoint: EndPointProtocol {
        var url: URL?
        var method: HTTPMethods = .get
    }
    
    init() {
        endPoint = FakeEndPoint()
    }
    
    func request(withObject object: Encodable?, completion: @escaping ((NetworkResult<Data?>) -> Void)) {
        guard let mockResponse = response else {
            fatalError("response of success and failure must be provided")
        }
        switch mockResponse {
        case .success(let jsonFileName):
            let data = JsonReader.createData(fromJSONFile: jsonFileName)
            completion(NetworkResult.success(data))
        case .failure(let jsonFileName):
            let errorData = JsonReader.createData(fromJSONFile: jsonFileName)
            let parsedError = parseErrorMessage(fromData: errorData)
            completion(NetworkResult.failure(parsedError))
        }
    }
    
    private func parseErrorMessage(fromData data: Data) -> Error {
        do {
            let decoder = JSONDecoder()
            let decodedResult = try decoder.decode(ApiErrorModel.self, from: data)
            return NetworkError.apiError(apiMessage: decodedResult.errorDescription, code: "")
        } catch let error {
            return NetworkError.apiError(apiMessage: error.localizedDescription, code: "")
        }
    }
    
}
