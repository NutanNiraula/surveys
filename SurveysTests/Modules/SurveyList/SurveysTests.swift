//
//  SurveysTests.swift
//  SurveysTests
//
//  Created by Nutan Niraula on 5/28/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import XCTest
@testable import Surveys

class SurveysTests: BaseXCTest {
    
    var sut: SurveysViewModel!

    override func setUp() {
        sut = SurveysViewModel(surveyListAPIEndPoint: SurveyListEndPoint(),
                               apiClientFactory: APIClientFactory(callerType: .mockHttp(mockCaller: mockAPICient)))
    }

    override func tearDown() {
        sut = nil
    }
    
    func test_getSurveyList_whenAPICallIsSuccessful_emitsModelInObserver() {
        mockAPICient.response = .success(jsonFileName: "SurveyListSuccess")
        //this test can be done in async manner but mock classes emit result instantly so it is done without waiting
        sut.surveysObserver = { surveys in
            XCTAssertEqual(surveys.count, 5)
            XCTAssertEqual(surveys[0].id, "e81ca9000387e953ef2d")
            XCTAssertEqual(surveys[0].title, "Tree Tops Australia")
            XCTAssertEqual(surveys[0].description, "We'd love to hear from you!")
        }
        sut.getSurveyList()
    }
    
    func test_getSurveyList_whenAPICallIsUnSuccessful_emitsErrorMessageInObserver() {
        mockAPICient.response = .failure(jsonFileName: "SurveyListFailure")
        mockAPICient.code = 401
        //this test can be done in async manner but mock classes emit result instantly so it is done without waiting
        sut.alertMessage = { message in
            // swiftlint:disable line_length
            let expected = "The request is missing a required parameter, includes an unsupported parameter value, or is otherwise malformed."
            // swiftlint:enable line_length
            XCTAssertEqual(message, expected)
        }
        sut.getSurveyList()
    }
    
    func test_getSurveyList_whenAPIIsCalledSubsequently_emitsModelByAppendingNewResultsInObserver() {
        mockAPICient.response = .success(jsonFileName: "SurveyListSuccess")
        //this test can be done in async manner but mock classes emit result instantly so it is done without waiting
        sut.surveysObserver = { _ in }
        sut.getSurveyList()
        sut.surveysObserver = { surveys in
            XCTAssertEqual(surveys.count, 10)
        }
        sut.getSurveyList()
    }
    
    func test_getSurveyList_whenAPIIsCalledSubsequently_addsPageToLoadBy1() {
        mockAPICient.response = .success(jsonFileName: "SurveyListSuccess")
        //this test can be done in async manner but mock classes emit result instantly so it is done without waiting
        sut.surveysObserver = { _ in }
        sut.getSurveyList()
        XCTAssertEqual(sut.pageToLoad, 2)
    }

    func test_resetPage_whenInvoked_resetsPageToLoad() {
        mockAPICient.response = .success(jsonFileName: "SurveyListSuccess")
        //this test can be done in async manner but mock classes emit result instantly so it is done without waiting
        sut.surveysObserver = { _ in }
        sut.getSurveyList()
        XCTAssertEqual(sut.pageToLoad, 2)
        sut.resetPage()
        XCTAssertEqual(sut.pageToLoad, 1)
    }
    
    func test_getSurveyList_whenAPIReturnsEmptyArray_setsIsLastPageToTrue() {
        mockAPICient.response = .success(jsonFileName: "SurveyListEmptyResponse")
        //this test can be done in async manner but mock classes emit result instantly so it is done without waiting
        XCTAssertFalse(sut.isLastPage)
        sut.surveysObserver = { _ in }
        sut.getSurveyList()
        XCTAssertTrue(sut.isLastPage)
    }
    
    func test_resetPage_whenInvoked_resetsIsLastPageToFalse() {
        mockAPICient.response = .success(jsonFileName: "SurveyListEmptyResponse")
        //this test can be done in async manner but mock classes emit result instantly so it is done without waiting
        sut.surveysObserver = { _ in }
        sut.getSurveyList()
        XCTAssertTrue(sut.isLastPage)
        sut.resetPage()
        XCTAssertFalse(sut.isLastPage)
    }

}
