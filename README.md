# Surveys

This app allows user to view various surveys and take the survey that they desire

## v0.1.0 Release Note

- surveys are be viewed in list form
- user can navigate to take survey page

## Architecture

- MVVM

## Pods used

- 'SwiftLint' - For code formatting 
- 'Alamofire' - For managing network requests
- 'SDWebImage' - For loading images asynchronously

## Folder/File Structure

- SurveysPrototype playground: This can be used for testing concepts and UI quickly

- Debug Tools Folder: This is used to store debug tools that can help during development, this will not be set to release target for production

- PlaygroundPrototypes Folder: This is where we add files to test and set its target to SurveysPlaygroundFramework so that playground can detect it

- Config Folder: various configuration files like info.plist, app mode controlling files and *.config file if multi schemes are created

- AppDelegate Folder:  Houses Appdelegate and AppCoordinator

- Helpers Folder: Managers, constants, UI components, network layer or in short helper classes used within the project, some of these can be taken out into framework as project progresses

- Utilities Folder: Protocols, extensions and helper functions that do not need complex architecture and can be easily wrapped in a struct or class

- Modules Folder: Main core of the app, each page of app is stored here. Storyboards, viewcontrollers, viewmodels, datamodels, coordinators, network endpoints used for app is stored here classified by feature

- Resources: Assets, localization files, audio files etc are stored here

## Targets

- Surveys main app : This can be broken down to multiple target if we want to add separate target for debug, staging, uat, production etc

- Surveys test target: I've only implemented tests in viewmodel assuming other manager classes are already tested.

- SurveysPlaygroundFramework: Framework imported by playground to test views and logical concepts



