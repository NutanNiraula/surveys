//
//  StoryboardIdentifiable.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

protocol StoryboardIdentifiable: ViewControllerIdentifiable {
    var name: String {get}
}

extension StoryboardIdentifiable {
    private func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.name, bundle: nil)
    }
    
    func viewController<T: UIViewController>(_: T.Type) -> T where T: ViewControllerIdentifiable {
        guard let viewController = storyboard().instantiateViewController(withIdentifier: T.name) as? T else {
            fatalError("ViewController couldn't be instantiated")
        }
        return viewController
    }
}
