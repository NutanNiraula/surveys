//
//  ViewControllerIdentifiable.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

protocol ViewControllerIdentifiable {}

extension ViewControllerIdentifiable where Self: UIViewController {
    static var name: String {
        return String(describing: self)
    }
}
