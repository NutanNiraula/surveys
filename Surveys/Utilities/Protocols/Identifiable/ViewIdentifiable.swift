//
//  ViewIdentifiable.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

protocol ViewIdentifiable: class {
    static var name: String { get }
}

extension ViewIdentifiable where Self: UIView {
    static var name: String {
        return String(describing: self)
    }
}
