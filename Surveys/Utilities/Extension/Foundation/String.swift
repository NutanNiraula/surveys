//
//  String.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

extension String {
    
    var localizedString: String {
        let bundle = Bundle.main
        return NSLocalizedString(self, bundle: bundle, comment: "")
    }
    
}
