//
//  Bool.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

extension Bool {
    
    var isFalse: Bool {
        return self == false
    }
    
}
