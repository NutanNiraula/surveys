//
//  Optional.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

extension Optional {
    
    var isNil: Bool {
        return self == nil
    }
    
    var hasValue: Bool {
        return self != nil
    }
    
}
