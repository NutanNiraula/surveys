//
//  UIScrollView.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/31/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

extension UIScrollView {
    func isAtBottom() -> Bool {
        guard self.contentOffset.y > 0 else {return false}
        let bottomDifference = Int(self.contentOffset.y + self.frame.size.height) - Int(self.contentSize.height)
        return bottomDifference.magnitude < 5
    }
}
