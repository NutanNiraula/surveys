//
//  UINavigationController.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func push(vc: UIViewController) {
        pushViewController(vc, animated: true)
    }
    
    func present(vc: UIViewController) {
        present(vc, animated: true, completion: nil)
    }
    
    func pop() {
        popViewController(animated: true)
    }
    
    func popToRoot() {
        popToRootViewController(animated: true)
    }
    
    func pop(to vc: UIViewController) {
        popToViewController(vc, animated: true)
    }
    
    func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
}
