//
//  UICollectionView.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func register<T: UICollectionViewCell>(_: T.Type) where T: ViewIdentifiable {
        let nib = UINib(nibName: T.name, bundle: nil)
        register(nib, forCellWithReuseIdentifier: T.name)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T
        where T: ViewIdentifiable {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.name, for: indexPath) as? T else {
            fatalError("Couldn't dequeue cell with identifier \(T.name)")
        }
        return cell
    }
    
}
