//
//  UIImageView.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func setImage(url: URL?) {
        guard let url = url else {
            image = #imageLiteral(resourceName: "placeholder.pdf")
            return
        }
        DispatchQueue.main.async {
            self.sd_imageTransition = SDWebImageTransition.fade
            self.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder.pdf"),
                             options: [SDWebImageOptions.scaleDownLargeImages, SDWebImageOptions.continueInBackground],
                             completed: nil)
        }
    }
    
}
