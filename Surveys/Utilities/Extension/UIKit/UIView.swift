//
//  UIView.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

extension UIView {
    
    func makeCircular() {
        layer.cornerRadius = frame.width/2
        clipsToBounds = true
    }
    
    func setBorder(withWidth width: CGFloat = 1, borderColor color: CGColor = UIColor.lightGray.cgColor) {
        layer.borderWidth = width
        layer.borderColor = color
    }
    
    func setCornerRadius(cornerRadius radius: CGFloat) {
        layer.cornerRadius = radius
        clipsToBounds = true
    }
    
    func addShadow(withColor: UIColor = .lightGray,
                   shadowOffset: CGSize = CGSize(width: 0.0, height: 0.0),
                   opacity: Float = 0.7,
                   shadowRadius: CGFloat = 2.0,
                   cornerRadius: CGFloat = 5.0) {
        layer.cornerRadius = cornerRadius
        layer.shadowColor = withColor.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = opacity
        layer.shadowRadius = shadowRadius
        layer.masksToBounds = false
    }
    
    func loadNib<T: UIView>(_: T.Type) where T: ViewIdentifiable {
        Bundle.main.loadNibNamed(T.name, owner: self, options: nil)
    }
    
    func loadNib(_ name: String) {
        Bundle.main.loadNibNamed(name, owner: self, options: nil)
    }
    
    func loadNibView<T: UIView>(_: T.Type, withOwner owner: Any) -> UIView where T: ViewIdentifiable {
        let nib = UINib(nibName: T.name, bundle: Bundle.main)
        guard let nibView = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            fatalError("Could not create view from nib")
        }
        return nibView
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func rotate(angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        let rotation = self.transform.rotated(by: radians)
        self.transform = rotation
    }
    
}
