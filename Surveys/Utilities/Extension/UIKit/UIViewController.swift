//
//  UIViewController.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(withTitle title: String,
                   message msg: String? = nil,
                   cancelTitle cancelTitleString: String = "ok_title".localizedString,
                   cancelButtonAction: @escaping () -> Void = {}) {
        let alertView = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alertView.addAction(UIAlertAction(title: cancelTitleString, style: .cancel, handler: { (_) in
            cancelButtonAction()
        }))
        self.present(alertView, animated: true, completion: nil)
    }
    
}
