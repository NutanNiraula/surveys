//
//  SurveysCoordinator.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

//Make a file template to quickly generate this
class SurveysCoordinator: Coordinator {
    
    var navigationController: UINavigationController
    
    required init(navigationController nc: UINavigationController) {
        navigationController = nc
    }
    
    func setSurveyAsInitialViewController() {
        let surveyVC = ViewRepo.Surveys.getSurveyVC()
        surveyVC.mainCoordinator = self
        navigationController.push(vc: surveyVC)
    }
    
    func navigateToTakeSurveyViewController(forSurveyId id: String) {
        let takeSurveyVC = ViewRepo.Surveys.getTakeSurveysVC(forId: id)
        takeSurveyVC.mainCoordinator = self
        navigationController.push(vc: takeSurveyVC)
    }
    
}
