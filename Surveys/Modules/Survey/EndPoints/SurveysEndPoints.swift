//
//  SurveysEndPoints.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

//SurveyList
struct SurveyListEndPoint: EndPointProtocol {
    var url = AppUrls.getAppUrl(fromPath: "surveys.json")
    var method: HTTPMethods = .get
}
