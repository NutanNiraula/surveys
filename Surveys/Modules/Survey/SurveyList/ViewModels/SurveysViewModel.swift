//
//  SurveysViewModel.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

//Make a file template to confirm BaseViewmodel
class SurveysViewModel: BaseViewModel {
    
    var surveysObserver: (([Surveys]) -> Void)!
    
    var isLastPage = false
    
    private var surveyListAPIClient: APIClient
    var pageToLoad = 1
    var surveys = [Surveys]()
    
    init(surveyListAPIEndPoint: SurveyListEndPoint, apiClientFactory: APIClientFactory = APIClientFactory()) {
        surveyListAPIClient = apiClientFactory.createNetworkCaller(endPoint: surveyListAPIEndPoint)
    }
    
    func getSurveyList() {
        showActivityIndicator()
        if isLastPage.isFalse {
            let requestModel = SurveysRequestModel(page: pageToLoad, perPage: 5)
            //We need to pull this structure into code snippet to generate it quickly
            surveyListAPIClient.request(withObject: requestModel) { [unowned self] (result) in
                self.hideActivityIndicator()
                switch result.decodeJson(toType: [Surveys].self) {
                case .success(let model):
                    if model.isEmpty {
                        self.isLastPage = true
                    } else {
                        self.surveys += model
                        self.surveysObserver(self.surveys)
                        self.pageToLoad += 1
                    }
                case .failure(let error):
                    self.alertMessage(error.localizedDescription)
                }
            }
        }
    }
    
    func resetPage() {
        surveys.removeAll(keepingCapacity: false)
        pageToLoad = 1
        isLastPage = false
    }
    
}
