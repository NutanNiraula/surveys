//
//  SurveysModel.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

//The model below does not map all the json properties
//Make this model complex as the project grows
struct Surveys: Decodable {
    
    var id: String
    var title: String
    var description: String
    var coverImageUrl: String
    
    var coverImageThumbnailUrl: URL? {
        return URL(string: coverImageUrl)
    }
    
    var coverImageHighResUrl: URL? {
        //appening 'l' to the url gets larger version of the image
        return URL(string: "\(coverImageUrl)l")
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, title, description
        case coverImageUrl = "cover_image_url"
    }
    
}
