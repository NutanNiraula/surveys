//
//  SurveysRequestModel.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

struct SurveysRequestModel: Encodable {
    var page = 1
    var perPage = 5
    
    private enum CodingKeys: String, CodingKey {
        case page
        case perPage = "per_page"
    }
}
