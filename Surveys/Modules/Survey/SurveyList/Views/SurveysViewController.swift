//
//  ViewController.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/28/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

//Make a file template to confirm BaseViewController
class SurveysViewController: BaseViewController {
    
    @IBOutlet var surveyCollectionView: EmptyIndicatingCollectionView!
    
    var viewModel: SurveysViewModel!
    var mainCoordinator: SurveysCoordinator?
    
    let cardHeight: CGFloat = {
        let mainScreenHeight = UIScreen.main.bounds.height
        let navBarHeight = UINavigationController().navigationBar.frame.height
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let excessHeight = navBarHeight + statusBarHeight
        return mainScreenHeight - excessHeight
    }()
    
    var surveyList = [Surveys]()
    lazy var pageView = UIPageControl()
    lazy var refreshButton = ViewRepo.Button.getRefreshButton()
    lazy var hamburgerButton = ViewRepo.Button.getHamburgerButton()
    
    override func viewDidLoad() {
        //Make a file template to not miss this
        super.baseViewModel = viewModel
        super.viewDidLoad()
        addPageView()
        addRefreshButtonAsLeftBarButtonItem()
        addHamburgerButtonAsRightBarButtonItem()
        localizeSubViews()
        setupSurveyCollectionView()
        observeSurveyList()
        viewModel.getSurveyList()
    }
    
    private func addHamburgerButtonAsRightBarButtonItem() {
        let uiBarButton = UIBarButtonItem.init(customView: hamburgerButton)
        navigationItem.rightBarButtonItem = uiBarButton
    }
    
    private func addRefreshButtonAsLeftBarButtonItem() {
        refreshButton.addTarget(self, action: #selector(onRefreshButtonTapped), for: .touchUpInside)
        let uiBarButton = UIBarButtonItem.init(customView: refreshButton)
        navigationItem.leftBarButtonItem = uiBarButton
    }
    
    @objc private func onRefreshButtonTapped() {
        viewModel.resetPage()
        viewModel.getSurveyList()
        guard surveyCollectionView.numberOfItems(inSection: 0) > 0 else { return }
        surveyCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        pageView.currentPage = 0
    }
    
    //not a good solution but works for now, took from stackoverflow
    //https://stackoverflow.com/questions/35842040/add-border-for-dots-in-uipagecontrol
    private func updatePageControl() {
        //recursion is bad because view hierarchy might change in new OS releases
        for (index, dot) in pageView.subviews.enumerated() {
            if index == pageView.currentPage {
                dot.backgroundColor = AppColors.white
                dot.layer.cornerRadius = dot.frame.size.height/2
            } else {
                dot.backgroundColor = UIColor.clear
                dot.layer.cornerRadius = dot.frame.size.height/2
                dot.layer.borderColor = AppColors.white.cgColor
                dot.layer.borderWidth = 1
            }
        }
    }
    
    private func addPageView() {
        pageView.isUserInteractionEnabled = false
        pageView.frame = CGRect(x: UIScreen.main.bounds.width/2 - 15,
        y: UIScreen.main.bounds.height/2 - 60, width: UIScreen.main.bounds.width, height: 30)
        pageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        pageView.rotate(angle: 90)
        view.addSubview(pageView)
    }
    
    private func observeSurveyList() {
        viewModel.surveysObserver = { [unowned self] surveys in
            self.pageView.numberOfPages = surveys.count
            self.surveyList = surveys
            self.surveyCollectionView.reloadData()
            self.updatePageControl()
        }
    }
    
    private func setupSurveyCollectionView() {
        surveyCollectionView.isPagingEnabled = true
        surveyCollectionView.showsVerticalScrollIndicator = false
        surveyCollectionView.register(SurveyCollectionViewCell.self)
        surveyCollectionView.delegate = self
        surveyCollectionView.dataSource = self
        surveyCollectionView.emptyCollectionViewDataSource = self
        surveyCollectionView.backgroundColor = AppColors.primary
    }
    
    private func localizeSubViews() {
        self.title = "surveys_title".localizedString
    }
    
    private func requestNextPage() {
        viewModel.getSurveyList()
    }

}

extension SurveysViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        let height = scrollView.bounds.size.height
        let currentPage = Int(ceil(yOffset/height))
        pageView.currentPage = currentPage
        //this is a hacky solution to render first page indicator correctly
        if currentPage == 1 {
            updatePageControl()
        }
        
        if scrollView == surveyCollectionView {
            if scrollView.isAtBottom() {
                if viewModel.isLastPage.isFalse {
                    viewModel.showActivityIndicator()
                    requestNextPage()
                }
            }
        }
        
    }
    
}

extension SurveysViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width, height: cardHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

extension SurveysViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return surveyList.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SurveyCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        cell.surveyModel = surveyList[indexPath.row]
        cell.takeSurveyButton.tag = indexPath.row
        cell.takeSurveyButton.addTarget(self, action: #selector(navigateToSurvey), for: .touchUpInside)
        return cell
    }
    
    @objc func navigateToSurvey(sender: UIButton) {
        mainCoordinator?.navigateToTakeSurveyViewController(forSurveyId: surveyList[sender.tag].id)
    }
    
}

extension SurveysViewController: EmptyCollectionViewIndicatorDataSource {
    
    func viewForEmptyCollectionView(_ tableView: UICollectionView) -> UIView {
        return ViewRepo.CollectionView.getEmptyIndicatingLabel(withMessage: "emptySurvey_message".localizedString)
    }
    
}
