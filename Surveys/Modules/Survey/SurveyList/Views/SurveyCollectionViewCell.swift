//
//  SurveyCollectionViewCell.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

class SurveyCollectionViewCell: UICollectionViewCell, ViewIdentifiable {

    @IBOutlet var titleLabel: H0Label!
    @IBOutlet var descriptionLabel: R1Label!
    @IBOutlet var surveyImageView: UIImageView!
    @IBOutlet var takeSurveyButton: ActionButton!
    @IBOutlet var overlayView: UIView!
    
    var surveyModel: Surveys? {
        didSet {
            guard let survey = surveyModel else { return }
            titleLabel.text = survey.title
            descriptionLabel.text = survey.description
            surveyImageView.setImage(url: survey.coverImageHighResUrl)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        localizeSubViews()
        setupSubViews()
    }
    
    private func localizeSubViews() {
        takeSurveyButton.setTitle("takeSurvey_button".localizedString, for: [])
    }
    
    private func setupSubViews() {
        surveyImageView.contentMode = .scaleAspectFill
        titleLabel.textAlignment = .center
        titleLabel.textColor = AppColors.white
        titleLabel.numberOfLines = 3
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 3
        descriptionLabel.textColor = AppColors.white
        //rendering alpha is performance intensive if possible get tinted image from backend
        overlayView.backgroundColor = AppColors.primary.withAlphaComponent(0.4)
    }

}
