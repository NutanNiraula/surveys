//
//  TakeSurveyViewController.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/31/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

class TakeSurveyViewController: BaseViewController {
    
    @IBOutlet var surveyIdLabel: H1Label!
    
    var viewModel: TakeSurveyViewModel!
    var mainCoordinator: SurveysCoordinator?

    override func viewDidLoad() {
        super.baseViewModel = viewModel
        super.viewDidLoad()
        self.title = "takeSurvey_title".localizedString
        surveyIdLabel.textAlignment = .center
        surveyIdLabel.text = "\("surveyId_text".localizedString) \(viewModel.surveyId)"
    }

}
