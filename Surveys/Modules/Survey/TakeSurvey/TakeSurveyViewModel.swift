//
//  TakeSurveyViewModel.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/31/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

class TakeSurveyViewModel: BaseViewModel {
    
    var surveyId: String
    
    init(withSurVeyId id: String) {
        surveyId = id
    }
    
}
