//
//  DataLogger.swift
//  Logger
//
//  Created by Nutan Niraula on 1/9/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

enum LoggerPrintModes: String {
    case minimal
//    case normal
    case verbose
}

struct LoggerConfig {
    var identifier: String
    var isEnabled: Bool
    var printMode: LoggerPrintModes
}
