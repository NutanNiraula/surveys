//
//  Log.swift
//  Logger
//
//  Created by Nutan Niraula on 1/10/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

class Log {
    
    private static var debugPrintConfiguration = LoggerConfig(identifier: "---DEBUG",
                                                              isEnabled: true,
                                                              printMode: .minimal)
    
    private static var errorPrintConfiguration = LoggerConfig(identifier: "---ERROR",
                                                              isEnabled: true,
                                                              printMode: .verbose)
    
    private static var jsonPrintConfiguration = LoggerConfig(identifier: "---JSON",
                                                             isEnabled: true,
                                                             printMode: .minimal)
    
    private static var deinitPrintConfiguration = LoggerConfig(identifier: "---DEINIT",
                                                               isEnabled: true,
                                                               printMode: .minimal)
    
    private static var todoPrintConfiguration = LoggerConfig(identifier: "---TODO",
                                                             isEnabled: true,
                                                             printMode: .minimal)
    
    private static let debugLogger = DebugLogger()
    
    private static let errorLogger = ErrorLogger()
    
    private static let jsonLogger = JsonLogger()
    
    private static let deinitLogger = DeinitLogger()
    
    private static let todoLogger = ToDoLogger()
    
    private static var debugLogObservable: ((String) -> Void)?
    
    private static var errorLogObservable: ((String) -> Void)?
    
    private (set) static var debugLog = ""
    
    class func logDebugData() {
        Log.debugLogObservable = { log in
            debugLog += "\(log) \n"
        }
    }
    
    class func logErrorData() {
        Log.errorLogObservable = { log in
            print("saved log is \(log)")
        }
    }
    
    class func d(filename fn: String = #file,
                 line ln: Int = #line,
                 funcName fnn: String = #function,
                 tag tg: String = "") {
        let tagStr: String? = tg.isEmpty ? nil : "+TAG : \(tg)"
        let fileName = getFileName(fromPath: fn)
        if let logObservable = debugLogObservable {
            logObservable(deinitLogger.log(info: "",
                                           loggerConfig: deinitPrintConfiguration,
                                           filename: fileName,
                                           line: ln,
                                           funcName: fnn,
                                           tag: tagStr,
                                           timeStamp: getTimeStamp()))
            return
        }
        
        deinitLogger.log(info: "",
                         loggerConfig: deinitPrintConfiguration,
                         filename: fileName,
                         line: ln,
                         funcName: fnn,
                         tag: tg,
                         timeStamp: getTimeStamp())
    }
    
    class func d(_ object: CustomDebugStringConvertible,
                 filename fn: String = #file,
                 line ln: Int = #line,
                 funcName fnn: String = #function,
                 tag tg: String = "") {
        
        //        let debugLogManager = LogManager(loggerType: debugLogger)
        let tagStr: String? = tg.isEmpty ? nil : "+TAG : \(tg)"
        let fileName = getFileName(fromPath: fn)
        if let logObservable = debugLogObservable {
            logObservable(debugLogger.log(info: object,
                                          loggerConfig: debugPrintConfiguration,
                                          filename: fileName,
                                          line: ln,
                                          funcName: fnn,
                                          tag: tagStr,
                                          timeStamp: getTimeStamp()))
            return
        }
        
        debugLogger.log(info: object,
                        loggerConfig: debugPrintConfiguration,
                        filename: fileName,
                        line: ln,
                        funcName: fnn,
                        tag: tg,
                        timeStamp: getTimeStamp())
        
    }
    
    class func e(_ errorMsg: String,
                 filename fn: String = #file,
                 line ln: Int = #line,
                 funcName fnn: String = #function,
                 tag tg: String = "") {
        //        let errorLogManager = LogManager(loggerType: ErrorLogger())
        let tagStr: String? = tg.isEmpty ? nil : "+TAG : \(tg)"
        let fileName = getFileName(fromPath: fn)
        if let logObservable = errorLogObservable {
            logObservable(errorLogger.log(info: errorMsg,
                                          loggerConfig: errorPrintConfiguration,
                                          filename: fileName,
                                          line: ln,
                                          funcName: fnn,
                                          tag: tagStr,
                                          timeStamp: getTimeStamp()))
            return
        }
        
        errorLogger.log(info: errorMsg,
                        loggerConfig: errorPrintConfiguration,
                        filename: fileName,
                        line: ln,
                        funcName: fnn,
                        tag: tg,
                        timeStamp: getTimeStamp())
        
    }
    
    class func td(_ todoTask: String,
                  filename fn: String = #file,
                  line ln: Int = #line,
                  funcName fnn: String = #function,
                  tag tg: String = "") {
        
        let tagStr: String? = tg.isEmpty ? nil : "+TAG : \(tg)"
        let fileName = getFileName(fromPath: fn)
        if let logObservable = errorLogObservable {
            logObservable(todoLogger.log(info: todoTask,
                                         loggerConfig: todoPrintConfiguration,
                                         filename: fileName,
                                         line: ln,
                                         funcName: fnn,
                                         tag: tagStr,
                                         timeStamp: getTimeStamp()))
            return
        }
        
        todoLogger.log(info: todoTask,
                       loggerConfig: todoPrintConfiguration,
                       filename: fileName,
                       line: ln,
                       funcName: fnn,
                       tag: tg,
                       timeStamp: getTimeStamp())
        
    }
    
    class func j(_ jsonData: Data,
                 filename fn: String = #file,
                 line ln: Int = #line,
                 funcName fnn: String = #function,
                 tag tg: String = "") {
        
        let tagStr: String? = tg.isEmpty ? nil : "+TAG : \(tg)"
        let fileName = getFileName(fromPath: fn)
        guard let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) else {
            Log.e("---JSON couldn't be parsed to string")
            return
        }
        if let logObservable = errorLogObservable {
            logObservable(jsonLogger.log(info: jsonString,
                                         loggerConfig: jsonPrintConfiguration,
                                         filename: fileName,
                                         line: ln,
                                         funcName: fnn,
                                         tag: tagStr,
                                         timeStamp: getTimeStamp()))
            return
        }
        
        jsonLogger.log(info: jsonString,
                       loggerConfig: jsonPrintConfiguration,
                       filename: fileName,
                       line: ln,
                       funcName: fnn,
                       tag: tg,
                       timeStamp: getTimeStamp())
        
    }
    
    private static func getTimeStamp() -> String {
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        return formatter.string(from: now)
    }
    
    private static func getFileName(fromPath path: String) -> String {
        let pathComponent = path.components(separatedBy: "/")
        return pathComponent.last ?? "N/A"
    }
    
}
