//
//  Loggers.swift
//  Logger
//
//  Created by Nutan Niraula on 1/10/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//
//swiftlint:disable line_length
//swiftlint:disable function_parameter_count
//I'm aware about the repitition of code and that strategies may not be proper design pattern for this or at least the way I'm implementing it still doesn't make complete sense. These repetitive codes are here in the speculation of possibilities that might arise during the library's test for the need of unique properties per logger, for example: debug logger may need separate initializer for logging additional data. This architetcure is designed to evolve later into analytics manager and also debug console.

import Foundation

protocol Logger {
    @discardableResult func log(info: CustomDebugStringConvertible,
                                loggerConfig config: LoggerConfig,
                                filename name: String,
                                line ln: Int,
                                funcName fn: String,
                                tag tg: String?,
                                timeStamp ts: String) -> String
}

class DebugLogger: Logger {
    
    @discardableResult
    func log(info: CustomDebugStringConvertible,
             loggerConfig config: LoggerConfig,
             filename name: String,
             line ln: Int,
             funcName fn: String,
             tag tg: String?,
             timeStamp ts: String) -> String {
        
        return LogBuilder.buildLog(info: info,
                                   loggerConfig: config,
                                   filename: name,
                                   line: ln,
                                   funcName: fn,
                                   tag: tg,
                                   timeStamp: ts)
    }
    
}

class ErrorLogger: Logger {
    
    @discardableResult
    func log(info: CustomDebugStringConvertible,
             loggerConfig config: LoggerConfig,
             filename name: String,
             line ln: Int,
             funcName fn: String,
             tag tg: String?,
             timeStamp ts: String) -> String {
        
        return LogBuilder.buildLog(info: info,
                                   loggerConfig: config,
                                   filename: name,
                                   line: ln,
                                   funcName: fn,
                                   tag: tg,
                                   timeStamp: ts)
    }
    
}

class JsonLogger: Logger {
    
    @discardableResult
    func log(info: CustomDebugStringConvertible,
             loggerConfig config: LoggerConfig,
             filename name: String,
             line ln: Int,
             funcName fn: String,
             tag tg: String?,
             timeStamp ts: String) -> String {
        
        return LogBuilder.buildLog(info: info,
                                   loggerConfig: config,
                                   filename: name,
                                   line: ln,
                                   funcName: fn,
                                   tag: tg,
                                   timeStamp: ts)
    }
    
}

class DeinitLogger: Logger {
    
    @discardableResult
    func log(info: CustomDebugStringConvertible,
             loggerConfig config: LoggerConfig,
             filename name: String,
             line ln: Int,
             funcName fn: String,
             tag tg: String?,
             timeStamp ts: String) -> String {
        
        var debugString = ""
        if config.isEnabled {
            switch config.printMode {
            case .minimal:
                debugString = "\(config.identifier) M \(tg ?? "") :: \(name) deinitialized \n"
            case .verbose:
                debugString = "\(config.identifier) Vb \(tg ?? "") :: \(info) FILE: \(name) deinitialized at TIMESTAMP: \(ts) \n"
            }
            print(debugString)
        }
        return debugString
    }
    
}

class ToDoLogger: Logger {
    
    @discardableResult
    func log(info: CustomDebugStringConvertible,
             loggerConfig config: LoggerConfig,
             filename name: String,
             line ln: Int,
             funcName fn: String,
             tag tg: String?,
             timeStamp ts: String) -> String {
        
        var debugString = ""
        if config.isEnabled {
            switch config.printMode {
            case .minimal:
                debugString = "\(config.identifier) M \(tg ?? "") :: \(info)\n"
            case .verbose:
                debugString = "\(config.identifier) Vb \(tg ?? "") :: \(info) FILE: \(name) LINE: \(ln) FUNCTION: \(fn)\n"
            }
            print(debugString)
        }
        return debugString
    }
    
}

fileprivate class LogBuilder {
    static func buildLog(info: CustomDebugStringConvertible,
                         loggerConfig config: LoggerConfig,
                         filename name: String,
                         line ln: Int,
                         funcName fn: String,
                         tag tg: String?,
                         timeStamp ts: String) -> String {
        var debugString = ""
        if config.isEnabled {
            switch config.printMode {
            case .minimal:
                debugString = "\(config.identifier) M \(tg ?? "") :: \(info) \n"
            case .verbose:
                debugString = "\(config.identifier) Vb \(tg ?? "") :: \(info) FILE: \(name) LINE: \(ln) FUNCTION: \(fn) TIMESTAMP: \(ts) \n"
            }
            #if DEBUG
            print(debugString)
            #endif
        }
        return debugString
    }
}
//swiftlint:enable function_parameter_count
//swiftlint:enable line_length
