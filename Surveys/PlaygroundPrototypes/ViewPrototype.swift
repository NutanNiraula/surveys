//
//  ViewPrototype.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/28/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

//This file should be used for creating custom views to test live in playground
public class TestView: UIView {
    
    private let cornerRadius: CGFloat = 20
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        styleView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func styleView() {
        backgroundColor = .lightGray
        layer.cornerRadius = cornerRadius
    }
    
}
