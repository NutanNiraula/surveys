//
//  AppDelegate.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/28/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        initializeDependencies()
        return true
    }
    
    private func initializeDependencies() {
        setNavigationbarDefaults()
        setAppCoordinator()
    }
    
    private func setNavigationbarDefaults() {
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = AppColors.white
        UINavigationBar.appearance().barTintColor = AppColors.primary
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: AppColors.white]
    }

    private func setAppCoordinator() {
        let mainNavController = UINavigationController()
        appCoordinator = AppCoordinator(navigationController: mainNavController)
        appCoordinator?.setInitialPage()
    }
    
}
