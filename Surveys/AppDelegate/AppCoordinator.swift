//
//  AppCoordinator.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    var navigationController: UINavigationController?
    
    var surveyCoordinator: SurveysCoordinator?
    var surveyNavigationController: UINavigationController?
    
    var appDelegate: AppDelegate {
        guard let appDelegator = Utility.appDelegate else {
            fatalError("Couldn't find app delegate")
        }
        return appDelegator
    }
    
    init(navigationController nc: UINavigationController) {
        navigationController = nc
    }
    
    func setInitialPage() {
        showHomePage()
    }
    
    private func showTestPage() {
        // show prototyping page if needed
    }
    
    private func showPageBeingDesigned() {
        //show ui of page being designed to avoid multiple clicks to reach the design
        let inDesignController = UINavigationController(rootViewController: ViewRepo.Surveys.getSurveyVC())
        appDelegate.window?.rootViewController = inDesignController
        appDelegate.window?.makeKeyAndVisible()
    }

    private func showHomePage() {
        surveyNavigationController = UINavigationController()
        surveyCoordinator = SurveysCoordinator(navigationController: surveyNavigationController!)
        surveyCoordinator!.setSurveyAsInitialViewController()
        appDelegate.window?.rootViewController = surveyNavigationController
        appDelegate.window?.makeKeyAndVisible()
    }

}
