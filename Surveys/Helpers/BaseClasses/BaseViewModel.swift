//
//  BaseViewModel.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

class BaseViewModel {
    
    var alertMessage: ((String) -> Void)!
    var showActivityIndicator: (() -> Void) = {}
    var hideActivityIndicator: (() -> Void) = {}
    
}
