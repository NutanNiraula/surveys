//
//  BaseViewController.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, BaseViewControllerProtocol {
    
    var baseViewModel = BaseViewModel()
    var activityIndicatorView = ActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeErrorMessage()
        observeHideActivityIndicatorCommand()
        observeShowActivityIndicatorCommand()
        setViewControllerProperties()
    }
    
    private func setViewControllerProperties() {
        self.automaticallyAdjustsScrollViewInsets = true
    }
    
    private func observeShowActivityIndicatorCommand() {
        baseViewModel.showActivityIndicator = { [weak self] in
            self?.activityIndicatorView.show()
        }
    }
    
    private func observeHideActivityIndicatorCommand() {
        baseViewModel.hideActivityIndicator = { [weak self] in
            self?.activityIndicatorView.hide()
        }
    }
    
    private func observeErrorMessage() {
        baseViewModel.alertMessage = { [weak self] (message) in
            self?.showAlert(withTitle: message)
        }
    }
    
}
