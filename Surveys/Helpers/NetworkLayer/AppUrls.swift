//
//  AppUrls.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

struct AppUrls {
    
    #if DEBUG
    private static let urlBuilder = URLBuilder().set(scheme: "https").set(host: "nimble-survey-api.herokuapp.com")
    #else
    //TODO: Change url scheme using xcode build configuration and schemes
    private static let urlBuilder = URLBuilder().set(scheme: "https")
                                                        .set(host: "nimble-survey-api.herokuapp.com")
    #endif

    static func getAppUrl(fromPath path: String) -> URL? {
        return AppUrls.urlBuilder.set(path: "\(path)").build()
    }
    
    static var RefreshUrl: URL? {
        return AppUrls.getAppUrl(fromPath: "oauth/token")
    }
    
}
