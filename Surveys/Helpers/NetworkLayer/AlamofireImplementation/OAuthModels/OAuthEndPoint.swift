//
//  EndPoints.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

struct OAuthEndPoint: EndPointProtocol {
    var url: URL? = AppUrls.getAppUrl(fromPath: "oauth/token")
    var method: HTTPMethods = .post
}
