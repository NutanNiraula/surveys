//
//  LoginRequestModel.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

//This should be provided in login page and should never be hard coded like done here
//In this code test I believe I'm allowed to use only survey page and the page to navigate to
//So, I'm hardcoding this
struct OAuthRequestModel: Encodable {
    var username = "carlos@nimbl3.com"
    var password = "antikera"
    var grantType = "password"
    
    private enum CodingKeys: String, CodingKey {
        case username, password
        case grantType = "grant_type"
    }
}
