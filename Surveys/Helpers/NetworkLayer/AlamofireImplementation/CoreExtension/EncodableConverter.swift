//
//  EncodableConverter.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

extension Encodable {
    
    func getRequestParameters() -> [String: Any]? {
        let jsonEncoder = JSONEncoder()
        if let jsonData = try? jsonEncoder.encode(self) {
            do {
                return  try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
                return nil
            }
        }
        return nil
    }
    
}
