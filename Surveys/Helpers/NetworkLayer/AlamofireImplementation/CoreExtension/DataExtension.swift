//
//  DataExtension.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

extension Data {
    
    //TODO: create separate logger class to manage json printing
    func prettyPrintedJson() -> String {
        if let json = try? JSONSerialization.jsonObject(with: self,
                                                        options: .mutableContainers),
            let jsonData = try? JSONSerialization.data(withJSONObject: json,
                                                       options: .prettyPrinted) {
            return String(decoding: jsonData, as: UTF8.self)
        } else {
            return "Invalid JSON data"
        }
    }
    
}
