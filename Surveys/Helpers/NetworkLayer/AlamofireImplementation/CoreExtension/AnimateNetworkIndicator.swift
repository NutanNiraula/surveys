//
//  AnimateNetworkIndicator.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

extension APIClient {
    
    func networkIndicator(shouldAnimate: Bool = true) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = shouldAnimate
    }
    
}
