//
//  NetworkResultParser.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

extension NetworkResult where Value == Data? {
    
    func decodeJson<ResponseType: Decodable>(toType: ResponseType.Type) -> NetworkResult<ResponseType> {
        switch self {
        case .success(let data):
            do {
                guard let data = data else {
                    //swiftlint:disable line_length
                    return NetworkResult<ResponseType>.failure(NetworkError.apiError(apiMessage: "nilData".localizedString,
                                                                                     code: ""))
                    //swiftlint:enable line_length
                }
                let decoder = JSONDecoder()
                let decodedResult = try decoder.decode(ResponseType.self, from: data)
                return NetworkResult<ResponseType>.success(decodedResult)
            } catch let error {
                return NetworkResult<ResponseType>.failure(error)
            }
        case .failure(let error):
            return NetworkResult<ResponseType>.failure(error)
        }
    }
    
}
