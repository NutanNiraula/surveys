//
//  HTTPMethodExtension.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation
import Alamofire

extension HTTPMethods {
    
    var encodingType: ParameterEncoding {
        switch self {
        case .get:
            return URLEncoding.default
        case .post:
            return JSONEncoding.default
        case .put:
            return JSONEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    func alamofireEquivalentHTTPMethod() -> HTTPMethod {
        switch self {
        case .options:
            return HTTPMethod.options
        case .get:
            return HTTPMethod.get
        case .head:
            return HTTPMethod.head
        case .post:
            return HTTPMethod.post
        case .put:
            return HTTPMethod.put
        case .patch:
            return HTTPMethod.patch
        case .delete:
            return HTTPMethod.delete
        case .trace:
            return HTTPMethod.trace
        case .connect:
            return HTTPMethod.connect
        }
    }
    
}
