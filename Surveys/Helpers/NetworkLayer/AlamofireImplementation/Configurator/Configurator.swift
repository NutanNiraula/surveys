//
//  Configurator.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

class NetworkConfigurator {
    
    static let shared = NetworkConfigurator()
    let requestTimeOutInterval: Double = 120 //seconds
    let responseTimeOutInterval: Double = 120
    var sessionConfiguration: URLSessionConfiguration
    
    //can add init if other configuration is needed
    init() {
        sessionConfiguration = URLSessionConfiguration.default //ephemeral and background
        sessionConfiguration.timeoutIntervalForRequest = requestTimeOutInterval //secs
        sessionConfiguration.timeoutIntervalForResource = responseTimeOutInterval//secs
    }
    
}
