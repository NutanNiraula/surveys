//
//  NetworkCallerConstants.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

struct NetworkCallerConstants {
    
    static let DeviceType = "ios"
    
    struct UserDefaults {
        static let requestCache = "request_cache"
        static let accessToken = "access_token"
        static let refreshToken = "refresh_token"
        static let deviceToken = "device_token"
    }
    
    struct Headers {
        static let Authorization = "Authorization"
        static let ContentType = "Content-Type"
        static let DeviceId = "Device-Id"
        static let Platform = "Platform"
        static let Locale = "Locale"
    }

}
