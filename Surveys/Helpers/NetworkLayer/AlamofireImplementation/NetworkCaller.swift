//
//  NetworkCaller.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation
import Alamofire

class NetworkCaller: APIClient {
    
    var endPoint: EndPointProtocol
    var alamofireSessionManger: Alamofire.SessionManager!
    var code: Int?
    static let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    var appdelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    init(endPoint: EndPointProtocol) {
        self.endPoint = endPoint
        alamofireSessionManger = Alamofire.SessionManager(configuration:
            NetworkConfigurator.shared.sessionConfiguration)
        alamofireSessionManger.adapter = NetworkAdapter()
        alamofireSessionManger.retrier = NetworkRetrier()
    }
    
    func request(withObject object: Encodable? = nil, completion: @escaping ((NetworkResult<Data?>) -> Void)) {
        self.networkIndicator(shouldAnimate: true)
        guard let url = self.endPoint.url else {return}
        
        //Headers are set to nil here.
        //But we can easily add header using a header protocol with dictionary var and confirmed by request object
        let request = alamofireSessionManger.request(url,
                                                     method: self.endPoint.method.alamofireEquivalentHTTPMethod(),
                                                     parameters: object?.getRequestParameters(),
                                                     encoding: self.endPoint.method.encodingType,
                                                     headers: [:]).validate(statusCode: 200...299)
        
        Log.d("*** API REQUEST *** \n REQUEST URL: \(String(describing: request.request))")
        Log.d("REQUEST HEADERS: \(String(describing: request.request?.allHTTPHeaderFields))")
        Log.d("REQUEST BODY: \(String(describing: request.request?.httpBody?.prettyPrintedJson()))")
        //        print(request.request?.allHTTPHeaderFields)
        
        request.responseData { [weak self] (dataResponse) in
            switch dataResponse.result {
            case .success(let data):
                Log.d("\(data.prettyPrintedJson())")
                let code = dataResponse.response!.statusCode
                self?.code = code
                completion(NetworkResult.success(data))
            case .failure(let error):
                if 400 ... 499 ~= (self?.code ?? 999) {
                    guard let err = self?.parseErrorMessage(fromData: dataResponse.data ?? Data()) else { return }
                    completion(NetworkResult.failure(err))
                    return
                }
                completion(NetworkResult.failure(error))
            }
        }
        self.networkIndicator(shouldAnimate: false)
    }
    
    func expireSession() {
        //session expiry logic, maybe route to login page
    }
    
    func parseErrorMessage(fromData data: Data) -> Error {
        do {
            let decoder = JSONDecoder()
            let decodedResult = try decoder.decode(ApiErrorModel.self, from: data)
            return NetworkError.apiError(apiMessage: decodedResult.errorDescription, code: "")
        } catch let error {
            return NetworkError.apiError(apiMessage: error.localizedDescription, code: "")
        }
    }
    
}
