//
//  NetworkAdapter.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation
import Alamofire

class NetworkAdapter: RequestAdapter {
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequestConfig = urlRequest

        urlRequestConfig.setValue(NetworkCallerConstants.DeviceType,
                                  forHTTPHeaderField: NetworkCallerConstants.Headers.Platform)
        let token = UserDefaults.standard.value(forKey: NetworkCallerConstants.UserDefaults.accessToken) as? String
        guard let authToken = token else {
            return urlRequestConfig
        }
        urlRequestConfig.setValue(String(format: "Bearer %@", authToken),
                                  forHTTPHeaderField: NetworkCallerConstants.Headers.Authorization)
        return urlRequestConfig
    }
}
