//
//  NetworkRetrier.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation
import Alamofire

enum RetryResult {
    var retryResult: Bool {
        switch self {
        case .failure, .networkError :
            return false
        default:
            return true
        }
    }
    case success
    case failure
    case networkError
}

class NetworkRetrier: RequestRetrier {
    private typealias RefreshCompletion = (_ succeeded: RetryResult) -> Void
    private let lock = NSLock()
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    private let alamofireManager = Alamofire.SessionManager(configuration:
        NetworkConfigurator.shared.sessionConfiguration)
    
    func should(_ manager: SessionManager,
                retry request: Request,
                with error: Error,
                completion: @escaping RequestRetryCompletion) {
        
        //If url is equal to login url retry should not be called
        if let response = request.task?.response as? HTTPURLResponse,
            response.statusCode == 401 ,
            request.retryCount < 1 && request.request?.url?.absoluteString != "Login url" {
            let prefs = UserDefaults.standard
            if let token = prefs.value(forKey: NetworkCallerConstants.UserDefaults.accessToken) as? String,
                let networkRequest = request.request,
                networkRequest.allHTTPHeaderFields![NetworkCallerConstants.Headers.Authorization] != "Bearer \(token)" {
                completion(true, 1.0)
            } else {
                requestsToRetry.append(completion)
                if !isRefreshing {
                    Log.d("Token expired trying to refresh")
                    refreshToken { [weak self] status in
                        guard let strongSelf = self else { return }
                        strongSelf.lock.lock(); defer { strongSelf.lock.unlock() }
                        switch status {
                        case .failure:
                            completion(false, 0.0)
                            self?.showAlertAndLogoutUser()
                        case .success: Log.d("Refresh Successs")
                        case .networkError: Log.d("Network Error while retrying")
                        }
                        strongSelf.requestsToRetry.forEach { $0(status.retryResult, 0.0) }
                        strongSelf.requestsToRetry.removeAll()
                        strongSelf.isRefreshing = false
                    }
                }
            }
        } else {
            completion(false, 0.0) //dont retry request
        }
    }
    
    private func refreshToken(completion: @escaping RefreshCompletion) {
        guard !isRefreshing, let refreshUrl = AppUrls.RefreshUrl  else { return }
        isRefreshing = true
        let requestModel = OAuthRequestModel()
        let request = alamofireManager.request(refreshUrl,
                                               method: .post,
                                               parameters: requestModel.getRequestParameters(),
                                               encoding: JSONEncoding.default,
                                               headers: [:])
        
        Log.d("*** API REQUEST *** \n REQUEST URL: \(String(describing: request.request))")
        Log.d("REQUEST HEADERS: \(String(describing: request.request?.allHTTPHeaderFields))")
        Log.d("REQUEST BODY: \(String(describing: request.request?.httpBody?.prettyPrintedJson()))")
        
        request.responseData { response in
            switch response.result {
            case .success(let data):
                if let obj = try? JSONDecoder().decode(OAuthResponseModel.self, from: data) {
                    //Save access token
                    UserDefaults.standard.set(obj.accessToken,
                                              forKey: NetworkCallerConstants.UserDefaults.accessToken)
                    UserDefaults.standard.synchronize()
                    completion(.success)
                } else {
                    Log.d("Session expired. Cannot refresh token")
                    completion(.failure)
                }
            case .failure(let error):
                Log.d("Session expired. Cannot refresh token: Message from failure block \(error.localizedDescription)")
                completion(.networkError)
            }
        }
    }
    
    func showAlertAndLogoutUser() {
        //clear app credentials
        //show alerts
        //route out of app with (session expired) message
    }
    
}
