//
//  NetworkError.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

struct ApiErrorModel: Decodable {
    var error: String
    var errorDescription: String
    
    private enum CodingKeys: String, CodingKey {
        case error
        case errorDescription = "error_description"
    }
}

//Custom error messages, these can be used for common error
enum NetworkError: Error {
    case notConnectedToInternet
    case timeOut
    case cancelled
    case badUrl
    case networkConnectionLost
    case networkResourceUnavailable
    case apiError(apiMessage: String, code: String)
    case cannotParseJsonError
    case noMobileDataAvailable //return 410 status code
}

extension NetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .notConnectedToInternet:
            break
        case .timeOut:
            break
        case .cancelled:
            break
        case .badUrl:
            break
        case .networkConnectionLost:
            break
        case .networkResourceUnavailable:
            break
        case .apiError(let apiMessage, _):
            return NSLocalizedString(apiMessage, comment: "came from api error")
        case .cannotParseJsonError:
            break
        case .noMobileDataAvailable:
            break
        }
        return NSLocalizedString("unknownError".localizedString, comment: "came from localized custom error")
    }
}
