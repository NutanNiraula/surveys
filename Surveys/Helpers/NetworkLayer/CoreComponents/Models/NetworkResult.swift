//
//  NetworkResult.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

enum NetworkResult<Value> {
    case success(Value)
    case failure(Error)
}
