//
//  EmptyModels.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

//Use these for mapping to empty request and empty response
class EmptyBaseRequest: Encodable {}

class EmptyBaseResponse: Decodable {}
