//
//  APIClient.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

//TODO: Once core components are finalized extract these files to separate framework
protocol APIClient {
    var endPoint: EndPointProtocol { get set }
    var code: Int? { get set }
    func request(withObject object: Encodable?, completion: @escaping ((NetworkResult<Data?>) -> Void))
    //multipart request function can be added but it is not needed for the scope of this project
}
