//
//  EndPointProtocol.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/29/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

protocol EndPointProtocol {
    var url: URL? {get set}
    var method: HTTPMethods {get}
}
