//
//  APIClientFactory.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

enum APIClientTypes {
    case http
    case mockHttp(mockCaller: APIClient)
}

class APIClientFactory {
    
    var callerType: APIClientTypes
    
    init(callerType type: APIClientTypes = .http) {
        callerType = type
    }
    
    func createNetworkCaller(endPoint ep: EndPointProtocol) -> APIClient {
        switch callerType {
        case .http:
            return NetworkCaller(endPoint: ep)
        case .mockHttp(let mockCaller):
            return mockCaller
        }
    }
}
