//
//  Storyboard.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

enum Storyboard: String, StoryboardIdentifiable {

    var name: String {
        return self.rawValue
    }
    
    case survey = "Surveys"
    
}
