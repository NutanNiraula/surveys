//
//  ViewRepo.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

struct ViewRepo {
    
    struct CollectionView {
        static func getEmptyIndicatingLabel(withMessage msg: String) -> UIView {
            let emptyView = UIView()
            let emptyMessageLabel = EmptyIndicatingLabel(frame: CGRect(x: 30, y: -50,
                                                                       width: UIScreen.main.bounds.width - 60,
                                                                       height: 50))
            emptyMessageLabel.text = msg
            emptyMessageLabel.textColor = AppColors.white
            emptyView.addSubview(emptyMessageLabel)
            return emptyView
        }
    }
    
    struct Surveys {
        
        static func getSurveyVC() -> SurveysViewController {
            let vc = Storyboard.survey.viewController(SurveysViewController.self)
            vc.viewModel = SurveysViewModel(surveyListAPIEndPoint: SurveyListEndPoint())
            return vc
        }
        
        static func getTakeSurveysVC(forId id: String) -> TakeSurveyViewController {
            let vc = Storyboard.survey.viewController(TakeSurveyViewController.self)
            vc.viewModel = TakeSurveyViewModel(withSurVeyId: id)
            return vc
        }
        
    }
    
    struct Button {
        
        static func getRefreshButton() -> UIButton {
            let refreshButton = UIButton(type: .custom)
            refreshButton.tintColor = AppColors.white
            refreshButton.setImage(#imageLiteral(resourceName: "refreshIcon.pdf").withRenderingMode(.alwaysTemplate), for: .normal)
            refreshButton.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
            return refreshButton
        }
        
        static func getHamburgerButton() -> UIButton {
            let hamburgerButton = UIButton(type: .custom)
            hamburgerButton.tintColor = AppColors.white
            hamburgerButton.setImage(#imageLiteral(resourceName: "hamburger.pdf").withRenderingMode(.alwaysTemplate), for: .normal)
            hamburgerButton.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
            //Squishing image should never be done in app, I found incompatible size button so I'm doing it here
            hamburgerButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 18, bottom: 8, right: 0)
            return hamburgerButton
        }
        
    }
    
}
