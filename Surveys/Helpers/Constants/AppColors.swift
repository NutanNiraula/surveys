//
//  AppColors.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

struct AppColors {
    
    static let primary = UIColor(hexString: "131B38", alpha: 1.0)
    static let red = UIColor(hexString: "DF0023", alpha: 1.0)
    static let white = UIColor.white
    static let lightGray = UIColor.lightGray
    
}
