//
//  AppFonts.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

struct AppFonts {
    
    //Simple classification, can be elaborate depending on design system
    static let H0 = UIFont.boldSystemFont(ofSize: 30)
    static let H1 = UIFont.boldSystemFont(ofSize: 22)
    static let H2 = UIFont.boldSystemFont(ofSize: 20)
    static let H3 = UIFont.boldSystemFont(ofSize: 18)
    static let H4 = UIFont.boldSystemFont(ofSize: 16)
    static let H5 = UIFont.boldSystemFont(ofSize: 14)
    static let H6 = UIFont.boldSystemFont(ofSize: 12)
    
    static let R1 = UIFont.systemFont(ofSize: 22)
    static let R2 = UIFont.systemFont(ofSize: 18)
    static let R3 = UIFont.systemFont(ofSize: 16)
    static let R4 = UIFont.systemFont(ofSize: 14)
    static let R5 = UIFont.systemFont(ofSize: 12)
    static let R6 = UIFont.systemFont(ofSize: 10)
    
}
