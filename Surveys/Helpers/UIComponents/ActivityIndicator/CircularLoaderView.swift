//
//  CircularLoaderView.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

class CircularLoaderView: UIView {
    
    var rotationAnimation: CAAnimation = {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.fromValue = 0
        animation.toValue = 2 * Double.pi
        animation.duration = 1.0
        animation.repeatCount = .infinity
        return animation
    }()
    
    var circleLayer = CAShapeLayer()
    var innerCircleLayer = CAShapeLayer()
    var circularPath = UIBezierPath()
    var innerCircularPath = UIBezierPath()
    
    var strokeWidth: CGFloat = 5.0 {
        didSet {
            circleLayer.lineWidth = strokeWidth
            innerCircleLayer.lineWidth = strokeWidth
        }
    }
    
    var circleRadius: CGFloat {
        return frame.size.height / 2
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViewStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViewStyle()
    }
    
    private func setupViewStyle() {
        circularPath = UIBezierPath(arcCenter: CGPoint(x: bounds.width/2, y: bounds.height/2),
                                    radius: circleRadius,
                                    startAngle: CGFloat(-Double.pi/2),
                                    endAngle: CGFloat(3 * Double.pi/2),
                                    clockwise: true)
        
        circleLayer = CAShapeLayer()
        circleLayer.bounds = circularPath.cgPath.boundingBoxOfPath
        circleLayer.position = CGPoint(x: circularPath.cgPath.boundingBoxOfPath.midX,
                                       y: circularPath.cgPath.boundingBoxOfPath.midY)
        circleLayer.path = circularPath.cgPath
        circleLayer.strokeColor = AppColors.lightGray.cgColor
        circleLayer.fillColor = nil
        circleLayer.lineWidth = strokeWidth
        layer.addSublayer(circleLayer)
        
        innerCircularPath = UIBezierPath(arcCenter: CGPoint(x: bounds.width/2, y: bounds.height/2),
                                         radius: circleRadius,
                                         startAngle: CGFloat(5 * Double.pi / 4),
                                         endAngle: CGFloat(7 * Double.pi/4),
                                         clockwise: true)
        innerCircleLayer = CAShapeLayer()
        innerCircleLayer.bounds = circularPath.cgPath.boundingBoxOfPath
        innerCircleLayer.position = CGPoint(x: circularPath.cgPath.boundingBoxOfPath.midX,
                                            y: circularPath.cgPath.boundingBoxOfPath.midY)
        innerCircleLayer.path = innerCircularPath.cgPath
        innerCircleLayer.fillColor = nil
        innerCircleLayer.strokeColor = AppColors.primary.cgColor
        innerCircleLayer.lineWidth = strokeWidth
        
        layer.addSublayer(innerCircleLayer)
    }
    
    func startAnimating() {
        innerCircleLayer.add(rotationAnimation, forKey: "rotateAnimation")
    }
    
}
