//
//  ActivityIndicatorView.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

class ActivityIndicatorView: UIView, ViewIdentifiable {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var circularLoaderView: CircularLoaderView!
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0,
                                width: UIScreen.main.bounds.width,
                                height: UIScreen.main.bounds.height))
        setUpView()
        applyEffects()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpView() {
        let view = loadNibView(ActivityIndicatorView.self, withOwner: self)
        addSubview(view)
    }
    
    private func applyEffects() {
        contentView.layer.cornerRadius = 10.0
        contentView.center = CGPoint(x: UIScreen.main.bounds.width / 2,
                                     y: UIScreen.main.bounds.height / 2)
        self.backgroundColor = AppColors.primary.withAlphaComponent(0.5)
    }
    
    func show() {
        guard let appDel = Utility.appDelegate else {
            assertionFailure("failed to load app delegate")
            return
        }
        appDel.window?.addSubview(self)
        circularLoaderView.startAnimating()
    }
    
    override func didMoveToSuperview() {
        self.contentView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            self.contentView.transform = CGAffineTransform.identity
        })
    }
    
    func hide() {
        self.removeFromSuperview()
    }
    
}
