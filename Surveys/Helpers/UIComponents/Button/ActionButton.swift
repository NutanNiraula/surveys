//
//  ActionButton.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

class ActionButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViewStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViewStyle()
    }
    
    func setupViewStyle() {
        layer.cornerRadius = bounds.height/2
        backgroundColor = AppColors.red
        tintColor = AppColors.white
        titleLabel?.font = AppFonts.R1
    }
    
}
