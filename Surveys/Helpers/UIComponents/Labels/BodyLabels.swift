//
//  BodyLabels.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

class R1Label: BaseLabel {
    override func setupViewStyle() {
        font = AppFonts.R1
    }
}

class R2Label: BaseLabel {
    override func setupViewStyle() {
        font = AppFonts.R2
    }
}

class R3Label: BaseLabel {
    override func setupViewStyle() {
        font = AppFonts.R3
    }
}

class R4Label: BaseLabel {
    override func setupViewStyle() {
        font = AppFonts.R4
    }
}
