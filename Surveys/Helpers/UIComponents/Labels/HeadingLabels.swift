//
//  HeadingLabels.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import Foundation

class H0Label: BaseLabel {
    override func setupViewStyle() {
        font = AppFonts.H0
    }
}

class H1Label: BaseLabel {
    override func setupViewStyle() {
        font = AppFonts.H1
    }
}

class H2Label: BaseLabel {
    override func setupViewStyle() {
        font = AppFonts.H2
    }
}

class H3Label: BaseLabel {
    override func setupViewStyle() {
        font = AppFonts.H3
    }
}

class H4Label: BaseLabel {
    override func setupViewStyle() {
        font = AppFonts.H4
    }
}
