//
//  BaseLabel.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

class BaseLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViewStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViewStyle()
    }
    
    //override the function as per need
    func setupViewStyle() {}
    
}
