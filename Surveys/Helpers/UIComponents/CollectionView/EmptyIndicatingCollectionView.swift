//
//  EmptyIndicatingCollectionView.swift
//  Surveys
//
//  Created by Nutan Niraula on 5/30/19.
//  Copyright © 2019 Nutan Niraula. All rights reserved.
//

import UIKit

protocol EmptyCollectionViewIndicatorDataSource: class {
    func viewForEmptyCollectionView(_ collectionView: UICollectionView) -> UIView
}

//This class does not handle all the possible scenarios
//I created this just to be enough useful for collectionview with single column
class EmptyIndicatingCollectionView: UICollectionView {
    
    weak var emptyCollectionViewDataSource: EmptyCollectionViewIndicatorDataSource? {
        didSet {
            guard let dataSource = emptyCollectionViewDataSource else {
                Log.d("data source is nil")
                return
            }
            
            addEmptyView(withView: dataSource.viewForEmptyCollectionView(self))
            handleEmptyCollectionView()
        }
    }
    
    private var customView: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func addEmptyView(withView view: UIView) {
        if customView != nil { customView.removeFromSuperview() }
        customView = view
        customView.alpha = 0
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
    private func handleEmptyCollectionView() {
        if collectionViewHasSections() {
            animateViewVisibility(isVisible: isFalse(collectionViewHasItems()))
        } else {
            animateViewVisibility(isVisible: true)
        }
    }
    
    private func collectionViewHasItems() -> Bool {
        for section in 0...numberOfSections - 1 {
            if numberOfItems(inSection: section) > 0 {
                return true
            }
        }
        return false
    }
    
    private func collectionViewHasSections() -> Bool {
        return numberOfSections > 0
    }
    
    private func animateViewVisibility(isVisible visibility: Bool) {
        UIView.animate(withDuration: 0.1) { [weak self] in
            if let emptyMessageView = self?.customView {
                emptyMessageView.alpha = visibility ? 1 : 0
            }
        }
    }
    
    func isFalse(_ bool: Bool) -> Bool {
        return bool == false
    }
    
}

// MARK: CollectionView overrides
extension EmptyIndicatingCollectionView {
    
    override func reloadData() {
        super.reloadData()
        handleEmptyCollectionView()
    }
    
    override func deleteItems(at indexPaths: [IndexPath]) {
        super.deleteItems(at: indexPaths)
        handleEmptyCollectionView()
    }
    
    override func insertItems(at indexPaths: [IndexPath]) {
        super.insertItems(at: indexPaths)
        handleEmptyCollectionView()
    }
    
}
